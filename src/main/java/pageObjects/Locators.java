package pageObjects;

import gherkin.lexer.Fi;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Locators {

    WebDriver webDriver;
    private WebDriverWait wait;

    //Constructor
    public Locators(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(this.webDriver, this);
        wait = new WebDriverWait(this.webDriver, 20);
    }

    //<editor-fold defaultstate="collapsed" desc="Login">

    @FindBy(id = "j_username")
    private WebElement txtUsuario;

    @FindBy (id = "j_password")
    private WebElement txtPassword;

    @FindBy (xpath = "//input[@name='j_idt18']")
    private WebElement btnIngresar;

    @FindBy (xpath = "//input[@name='j_idt17']")
    private WebElement btnIngresar2;

    @FindBy (xpath = "//label[@id='logo']")
    private WebElement lblLogueoOk;

    @FindBy (xpath = "//a[@id='logo']")
    private WebElement lblLogueoOk2;

    @FindBy (xpath = "//span[@class='ui-messages-warn-icon']")
    private WebElement lblLogueoError;

    public void insertUsuario(String usuario){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(txtUsuario));
        txtUsuario.sendKeys(usuario);
    }

    public void insertPassword(String password){
        WebDriverWait wait= new WebDriverWait(webDriver,9000);
        wait.until(ExpectedConditions.visibilityOf(txtPassword));
        txtPassword.sendKeys(password);
    }

    public void  clickIngresar(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnIngresar));
        btnIngresar.click();
    }

    public void  clickIngresar2(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnIngresar2));
        btnIngresar2.click();
    }

    public Boolean buttonLogueoOk() {
        return wait.until(ExpectedConditions.visibilityOf(lblLogueoOk)).isDisplayed();
    }

    public Boolean buttonLogueoOk2() {
        return wait.until(ExpectedConditions.visibilityOf(lblLogueoOk2)).isDisplayed();
    }

    public Boolean buttonLogueoError() {
        return wait.until(ExpectedConditions.visibilityOf(lblLogueoError)).isDisplayed();
    }




    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Politicas de privacidad">
    @FindBy(xpath = "//a[contains(text(),'Pol\u00EDticas de Privacidad')]")
//    @FindBy(xpath = "/html[1]/body[1]/div[1]/div[1]/footer[1]/small[1]/a[2]")
    private WebElement btnPoliticasDePrivacidad;

    @FindBy(xpath = "//p[contains(text(),'Comsatel Per\u00FA SAC - Todos los derechos reservados')]")
//    @FindBy(xpath = "/html[1]/body[1]/section[2]/section[1]/p[1]")
    private WebElement lblPoliticasComsatel;

    @FindBy (xpath = "//div[@class='logo']//a//img")
    private WebElement toltip;

    public void clickPoliticasPrivacidad(){
        WebDriverWait wait= new WebDriverWait(webDriver, 5000);
        wait.until(ExpectedConditions.visibilityOf(btnPoliticasDePrivacidad));
        btnPoliticasDePrivacidad.click();
    }

    public String getToolTip(){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(toltip).perform();
        return toltip.getAttribute("title");
    }

    public String getPoliticasComsatel(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(lblPoliticasComsatel));

        Actions actions = new Actions(webDriver);
        actions.moveToElement(lblPoliticasComsatel);
        actions.perform();

        return lblPoliticasComsatel.getText();
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Sobre Comsatel">
    @FindBy(xpath = "//a[contains(text(),'Sobre Comsatel')]")
    private WebElement btnSobreComsatel;

    public void clickSobreComsatel(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnSobreComsatel));
        btnSobreComsatel.click();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Exportar CLSeguridad Asignación de flotas por usuario">
    @FindBy(xpath = "//div[@id='card-flotas-usuarios']//i[@class='material-icons points'][contains(text(),'more_horiz')]")
    private WebElement btnPuntosFlotasPorUsuarios;

    @FindBy(xpath = "//ul[@class='dropdown-menu dropdown-menu-right show showing']//a[contains(text(),'Exportar')]")
    private WebElement btnExportarFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='exportarFlotasUsuario:dlg_exportarFlotasUsuario']//a[1]//img[1]")
    private WebElement btnExportarExcelFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='exportarFlotasUsuario:dlg_exportarFlotasUsuario']//a[2]//img[1]")
    private WebElement btnExportarPdfFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='exportarFlotasUsuario:dlg_exportarFlotasUsuario']//span[@class='ui-icon ui-icon-closethick']")
    private WebElement btnCerrarFlotasPorUsuarios;

    public void clickPuntosFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnPuntosFlotasPorUsuarios));
        btnPuntosFlotasPorUsuarios.click();
    }

    public void clickExportarFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarFlotasPorUsuarios));
        btnExportarFlotasPorUsuarios.click();
    }

    public void clickExportarExcelFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarExcelFlotasPorUsuarios));
        btnExportarExcelFlotasPorUsuarios.click();
    }

    public void clickExportarPdfFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarPdfFlotasPorUsuarios));
        btnExportarPdfFlotasPorUsuarios.click();
    }

    public void clickCerrarFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnCerrarFlotasPorUsuarios));
        btnCerrarFlotasPorUsuarios.click();
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Exportar CLSeguridad Usuarios y vehículos por flota">
    @FindBy(xpath = "//div[@id='card-usuarios-flota']//i[@class='material-icons points'][contains(text(),'more_horiz')]")
    private WebElement btnPuntosUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//div[@id='card-usuarios-flota']//li[1]")
    private WebElement btnExportarUsuariosYVehiculosPorFlota;

    @FindBy(xpath = "//div[@id='exportarUsuarioFlotas:dlg_exportarUsuarioFlotas']//a[1]//img[1]")
    private WebElement btnExportarExcelUsuariosYVehiculosPorFlota;

    @FindBy(xpath = "//div[@id='exportarUsuarioFlotas:dlg_exportarUsuarioFlotas']//a[2]//img[1]")
    private WebElement btnExportarPdfUsuariosYVehiculosPorFlota;

    @FindBy(xpath = "//div[@id='exportarUsuarioFlotas:dlg_exportarUsuarioFlotas']//span[@class='ui-icon ui-icon-closethick']")
    private WebElement btnCerrarUsuariosYVehiculosPorFlota;

    public void clickPuntosUsuariosYVehiculosPorFlota(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnPuntosUsuariosYVehiculosPorFlotas));
        btnPuntosUsuariosYVehiculosPorFlotas.click();
    }

    public void clickExportarUsuariosYVehiculosPorFlota(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarUsuariosYVehiculosPorFlota));
        btnExportarUsuariosYVehiculosPorFlota.click();
    }

    public void clickExportarExcelUsuariosYVehiculosPorFlota(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarExcelUsuariosYVehiculosPorFlota));
        btnExportarExcelUsuariosYVehiculosPorFlota.click();
    }

    public void clickExportarPdfUsuariosYVehiculosPorFlota(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarPdfUsuariosYVehiculosPorFlota));
        btnExportarPdfUsuariosYVehiculosPorFlota.click();
    }

    public void clickCerrarUsuariosYVehiculosPorFlota(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnCerrarUsuariosYVehiculosPorFlota));
        btnCerrarUsuariosYVehiculosPorFlota.click();
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Exportar CLSeguridad Control de usuarios por turno">
    @FindBy(xpath = "//div[@id='card-control-usuarios']//i[@class='material-icons points'][contains(text(),'more_horiz')]")
    private WebElement btnPuntosControlUsuariosPorTurno;

    @FindBy(xpath = "//ul[@class='dropdown-menu dropdown-menu-right show showing']//a[contains(text(),'Exportar')]")
    private WebElement btnExportarControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='exportarUsuariosTurno:dlg_exportarUsuariosTurno']//a[1]//img[1]")
    private WebElement btnExportarExcelControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='exportarUsuariosTurno:dlg_exportarUsuariosTurno']//a[2]//img[1]")
    private WebElement btnExportarPdfControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='exportarUsuariosTurno:dlg_exportarUsuariosTurno']//span[@class='ui-icon ui-icon-closethick']")
    private WebElement btnCerrarControlUsuariosPorTurno;

    public void clickPuntosControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnPuntosControlUsuariosPorTurno));
        btnPuntosControlUsuariosPorTurno.click();
    }

    public void clickExportarControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarControlUsuariosPorTurno));
        btnExportarControlUsuariosPorTurno.click();
    }

    public void clickExportarExcelControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarExcelControlUsuariosPorTurno));
        btnExportarExcelControlUsuariosPorTurno.click();
    }

    public void clickExportarPdfControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarPdfControlUsuariosPorTurno));
        btnExportarPdfControlUsuariosPorTurno.click();
    }

    public void clickCerrarControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnCerrarControlUsuariosPorTurno));
        btnCerrarControlUsuariosPorTurno.click();
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Exportar CLSeguridad Control de perfiles por usuario">
    @FindBy(xpath = "//div[@id='card-perfiles-usuario']//i[@class='material-icons points'][contains(text(),'more_horiz')]")
    private WebElement btnPuntosControlPerfilesPorUsuario;

    @FindBy(xpath = "//ul[@class='dropdown-menu dropdown-menu-right show showing']//a[contains(text(),'Exportar')]")
    private WebElement btnExportarControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='exportarPerfilUsuario:dlg_exportarPerfilUsuario']//div[@class='ui-dialog-content ui-widget-content']//a[1]")
    private WebElement btnExportarExcelControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='exportarPerfilUsuario:dlg_exportarPerfilUsuario']//a[2]//img[1]")
    private WebElement btnExportarPdfControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='exportarPerfilUsuario:dlg_exportarPerfilUsuario']//span[@class='ui-icon ui-icon-closethick']")
    private WebElement btnCerrarControlPerfilesPorUsuario;

    public void clickPuntosControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnPuntosControlPerfilesPorUsuario));
        btnPuntosControlPerfilesPorUsuario.click();
    }

    public void clickExportarControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarControlPerfilesPorUsuario));
        btnExportarControlPerfilesPorUsuario.click();
    }

    public void clickExportarExcelControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarExcelControlPerfilesPorUsuario));
        btnExportarExcelControlPerfilesPorUsuario.click();
    }

    public void clickExportarPdfControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnExportarPdfControlPerfilesPorUsuario));
        btnExportarPdfControlPerfilesPorUsuario.click();
    }

    public void clickCerrarControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnCerrarControlPerfilesPorUsuario));
        btnCerrarControlPerfilesPorUsuario.click();
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Asignación de flotas por usuario">

    @FindBy(xpath = "//div[@id='card-flotas-usuarios']//li[2]")
    private WebElement btnEnviarFlotasPorUsuarios;

    @FindBy(xpath = "//input[@id='enviarFlotaUsuario:frmEnviar:autoContactos_input']")
    private WebElement txtEnviarFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='enviarFlotaUsuario:frmEnviar:autoContactos_panel']//tr[1]//td[1]")
    private WebElement btnEnviarSeleccionarFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='enviarFlotaUsuario:dlg_enviarFlotaUsuario']//li[1]//label[1]//img[1]")
    private WebElement btnEnviarExcelFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='enviarFlotaUsuario:dlg_enviarFlotaUsuario']//li[2]//label[1]//img[1]")
    private WebElement btnEnviarPdfFlotasPorUsuarios;

    @FindBy(xpath = "//div[@id='enviarFlotaUsuario:dlg_enviarFlotaUsuario']//button[1]")
    private WebElement btnEnviarCorreoFlotasPorUsuarios;


    public void clickEnviarFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarFlotasPorUsuarios));
        btnEnviarFlotasPorUsuarios.click();
    }

    public void insertUsuarioEnviarFlotasPorUsuarios(String usuario){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(txtEnviarFlotasPorUsuarios));
        txtEnviarFlotasPorUsuarios.sendKeys(usuario);
    }

    public void clickSeleccionarEnviarFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarSeleccionarFlotasPorUsuarios));
        btnEnviarSeleccionarFlotasPorUsuarios.click();
    }

    public void clickEnviarExcelFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarExcelFlotasPorUsuarios));
        btnEnviarExcelFlotasPorUsuarios.click();
    }
//
    public void clickEnviarPdfFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarPdfFlotasPorUsuarios));
        btnEnviarPdfFlotasPorUsuarios.click();
    }

    public void clickEnviarCorreoFlotasPorUsuarios(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarCorreoFlotasPorUsuarios));
        btnEnviarCorreoFlotasPorUsuarios.click();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Usuarios y vehículos por flota">

    @FindBy(xpath = "//div[@id='card-usuarios-flota']//li[2]")
    private WebElement btnEnviarUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//input[@id='enviarUsuarioFlota:frmEnviar:autoContactos_input']")
    private WebElement txtEnviarUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//tr[@class='ui-autocomplete-item ui-autocomplete-row ui-corner-all ui-state-highlight']//span[contains(text(),'GRIOS@COMSATEL.COM.PE')]")
    private WebElement btnEnviarSeleccionarUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//div[@id='enviarUsuarioFlota:dlg_enviarUsuarioFlota']//li[1]//label[1]//img[1]")
    private WebElement btnEnviarExcelUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//div[@id='enviarUsuarioFlota:dlg_enviarUsuarioFlota']//li[2]//label[1]//img[1]")
    private WebElement btnEnviarPdfUsuariosYVehiculosPorFlotas;

    @FindBy(xpath = "//div[@id='enviarUsuarioFlota:dlg_enviarUsuarioFlota']//button[1]")
    private WebElement btnEnviarCorreoUsuariosYVehiculosPorFlotas;


    public void clickEnviarUsuariosYVehiculosPorFlotas(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarUsuariosYVehiculosPorFlotas));
        btnEnviarUsuariosYVehiculosPorFlotas.click();
    }

    public void insertUsuarioEnviarUsuariosYVehiculosPorFlotas(String usuario){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(txtEnviarUsuariosYVehiculosPorFlotas));
        txtEnviarUsuariosYVehiculosPorFlotas.sendKeys(usuario);
    }

    public void clickSeleccionarEnviarUsuariosYVehiculosPorFlotas(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarSeleccionarUsuariosYVehiculosPorFlotas));
        btnEnviarSeleccionarUsuariosYVehiculosPorFlotas.click();
    }

    public void clickEnviarExcelUsuariosYVehiculosPorFlotas(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarExcelUsuariosYVehiculosPorFlotas));
        btnEnviarExcelUsuariosYVehiculosPorFlotas.click();
    }
    //
    public void clickEnviarPdfUsuariosYVehiculosPorFlotas(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarPdfUsuariosYVehiculosPorFlotas));
        btnEnviarPdfUsuariosYVehiculosPorFlotas.click();
    }

    public void clickEnviarCorreoUsuariosYVehiculosPorFlotas(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarCorreoUsuariosYVehiculosPorFlotas));
        btnEnviarCorreoUsuariosYVehiculosPorFlotas.click();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Control de usuarios por turno">

    @FindBy(xpath = "//div[@class='col-xl-2 col-lg-2 col-md-2 col-sm-2 show']//li[2]")
    private WebElement btnEnviarControlUsuariosPorTurno;

    @FindBy(xpath = "//input[@id='componentEnviarCorreo:frmEnviar:autoContactos_input']")
    private WebElement txtEnviarControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='componentEnviarCorreo:frmEnviar:autoContactos_panel']//tr[1]//td[1]")
    private WebElement btnEnviarSeleccionarControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='componentEnviarCorreo:dlg_componentEnviarCorreo']//li[1]//label[1]//img[1]")
    private WebElement btnEnviarExcelControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='componentEnviarCorreo:dlg_componentEnviarCorreo']//li[2]//label[1]//img[1]")
    private WebElement btnEnviarPdfControlUsuariosPorTurno;

    @FindBy(xpath = "//div[@id='componentEnviarCorreo:dlg_componentEnviarCorreo']//button[1]")
    private WebElement btnEnviarCorreoControlUsuariosPorTurno;


    public void clickEnviarControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarControlUsuariosPorTurno));
        btnEnviarControlUsuariosPorTurno.click();
    }

    public void insertUsuarioEnviarControlUsuariosPorTurno(String usuario){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(txtEnviarControlUsuariosPorTurno));
        txtEnviarControlUsuariosPorTurno.sendKeys(usuario);
    }

    public void clickSeleccionarEnviarControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarSeleccionarControlUsuariosPorTurno));
        btnEnviarSeleccionarControlUsuariosPorTurno.click();
    }

    public void clickEnviarExcelControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarExcelControlUsuariosPorTurno));
        btnEnviarExcelControlUsuariosPorTurno.click();
    }
    //
    public void clickEnviarPdfControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarPdfControlUsuariosPorTurno));
        btnEnviarPdfControlUsuariosPorTurno.click();
    }

    public void clickEnviarCorreoControlUsuariosPorTurno(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarCorreoControlUsuariosPorTurno));
        btnEnviarCorreoControlUsuariosPorTurno.click();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Control de perfiles por usuario">

    @FindBy(xpath = "//ul[@class='dropdown-menu dropdown-menu-right show showing']//a[contains(text(),'Enviar')]")
    private WebElement btnEnviarControlPerfilesPorUsuario;

    @FindBy(xpath = "//input[@id='enviarPerfilUsuario:frmEnviar:autoContactos_input']")
    private WebElement txtEnviarControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='enviarPerfilUsuario:frmEnviar:autoContactos_panel']//tr[1]//td[1]")
    private WebElement btnEnviarSeleccionarControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='enviarPerfilUsuario:dlg_enviarPerfilUsuario']//li[1]//label[1]//img[1]")
    private WebElement btnEnviarExcelControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='enviarPerfilUsuario:dlg_enviarPerfilUsuario']//li[2]//label[1]//img[1]")
    private WebElement btnEnviarPdfControlPerfilesPorUsuario;

    @FindBy(xpath = "//div[@id='enviarPerfilUsuario:dlg_enviarPerfilUsuario']//button[1]")
    private WebElement btnEnviarCorreoControlPerfilesPorUsuario;


    public void clickEnviarControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarControlPerfilesPorUsuario));
        btnEnviarControlPerfilesPorUsuario.click();
    }

    public void insertUsuarioEnviarControlPerfilesPorUsuario(String usuario){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(txtEnviarControlPerfilesPorUsuario));
        txtEnviarControlPerfilesPorUsuario.sendKeys(usuario);
    }

    public void clickSeleccionarEnviarControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarSeleccionarControlPerfilesPorUsuario));
        btnEnviarSeleccionarControlPerfilesPorUsuario.click();
    }

    public void clickEnviarExcelControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarExcelControlPerfilesPorUsuario));
        btnEnviarExcelControlPerfilesPorUsuario.click();
    }
    //
    public void clickEnviarPdfControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarPdfControlPerfilesPorUsuario));
        btnEnviarPdfControlPerfilesPorUsuario.click();
    }

    public void clickEnviarCorreoControlPerfilesPorUsuario(){
        WebDriverWait wait= new WebDriverWait(webDriver, 9000);
        wait.until(ExpectedConditions.visibilityOf(btnEnviarCorreoControlPerfilesPorUsuario));
        btnEnviarCorreoControlPerfilesPorUsuario.click();
    }

    //</editor-fold>




    //<editor-fold defaultstate="collapsed" desc="Validaciones">
    @FindBy (xpath = "//p[contains(text(),'Envío de correo exitoso')]")
    private WebElement lblEnvioExitoso;

    public void getEnvioExitoso(){
        WebDriverWait wait = new WebDriverWait(webDriver, 6000);
        wait.until(ExpectedConditions.visibilityOf(lblEnvioExitoso));
        lblEnvioExitoso.getText();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variables">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variables">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Variables">
    //</editor-fold>



}
