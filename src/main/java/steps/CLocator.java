package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import pageObjects.Locators;

import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class CLocator {

    WebDriver webDriver;

    Locators locators;

    public CLocator() {
        webDriver = new ChromeDriver();
        locators = new Locators(webDriver);
    }

    @Before
    public void startUp() {
    }



    //<editor-fold defaultstate="collapsed" desc="busqueda de elemento en pestañas">
    private void situarDriverEnPestana(){
        String subWindowHandler = null;
        Set<String> handles = webDriver.getWindowHandles();
        Iterator<String> iterator = handles.iterator();
        while (iterator.hasNext()){
            subWindowHandler = iterator.next();
        }
        webDriver.switchTo().window(subWindowHandler);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Carga de modulos CL">

    @Given("^Se carga el modulo de CL$")
    public void cargamoduloCL() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://clocatorplus-qa.comsatel.com.pe/CL");
    }
    @Given("^Se carga el modulo de CLSeguridad$")
    public void cargaModuloSeguridad() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLSeguridad");
    }
    @Given("^Se carga el modulo de CLMantenimiento$")
    public void cargaModuloMantenimiento() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLMantenimiento");
    }

    @Given("^Se carga el modulo de CLAlarma$")
    public void cargaModuloAlarma() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLAlarma");
    }
    @Given("^Se carga el modulo de CLHerramientaControl$")
    public void cargaModuloHerramientaControl() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLHerramientaControl");
    }
    @Given("^Se carga el modulo de CLReporte$")
    public void cargaModuloReporte() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLReporte");
    }

    @Given("^Se carga el modulo de CLRuta$")
    public void cargaModuloRuta() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLRuta");
    }
    @Given("^Se carga el modulo de CLKilometraje$")
    public void cargaModuloKilometraje() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLKilometraje");
    }

    @Given("^Se carga el modulo de CLDireccion$")
    public void cargaModuloDireccion() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLDireccion");
    }
    @Given("^Se carga el modulo de CLAccesorio$")
    public void cargaModuloAccesorio() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLAccesorio");
    }
    @Given("^Se carga el modulo de CLControlEquipo$")
    public void cargaModuloControlequipo() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLControlEquipo");
    }
    @Given("^Se carga el modulo de CLConvoy$")
    public void cargamoduloconvoy() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLConvoy");
    }
    @Given("^Se carga el modulo de CLSafe$")
    public void cargamodulosafe() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLSafe");
    }
    @Given("^Se carga el modulo de CLConductor$")
    public void cargaModuloConductor() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLConductor");
    }

    @Given("^Se carga el modulo de CLConductorKP$")
    public void cargaModuloConductorkp() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLConductorKP");
    }
    @Given("^Se carga el modulo de CLDashboard$")
    public void cargaModuloDashboard() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLDashboard");
    }
    @Given("^Se carga el modulo de CLCapa$")
    public void cargaModuloCapa() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLCapa");
    }
    @Given("^Se carga el modulo de CLComando$")
    public void cargaModulComando() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLComando");
    }
    @Given("^Se carga el modulo de CLControlVencimiento$")
    public void cargaModuloControlVencimiento() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLControlVencimiento");
    }
    @Given("^Se carga el modulo de CLRutaControlada$")
    public void cargaModuloRutaControlada() throws Throwable {
        webDriver.manage().window().maximize();
        webDriver.get("http://pruebasqa.comsatel.com.pe/CLRutaControlada");
    }


    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Logueo al modulo">



    @When("^Ingresamos el usuario \"([^\"]*)\" y password \"([^\"]*)\"$")
    public void insertUserandPassword(String usuario, String password) throws Throwable {
        locators.insertUsuario(usuario);
        locators.insertPassword(password);
        locators.clickIngresar();
    }

    @When("^2Ingresamos el usuario \"([^\"]*)\" y password \"([^\"]*)\"$")
    public void InsertUserandPassword2(String usuario, String password) throws Throwable {
        locators.insertUsuario(usuario);
        locators.insertPassword(password);
        locators.clickIngresar2();
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Validacion Logueo">



    @Then("^Se valida que se logueo exitosamente \"([^\"]*)\"$")
    public void validarLogueoExitoso (String ok) throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(locators.buttonLogueoOk(),"Login Correcto");

    }

    @Then("^2Se valida que se logueo exitosamente \"([^\"]*)\"$")
    public void validarLogueoExitoso2 (String ok) throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(locators.buttonLogueoOk2(),"Login Correcto");

    }

    @Then("^Se valida que muestra mensaje de error \"([^\"]*)\"$")
    public void validarLogueoerroneo (String error) throws Throwable {
        Thread.sleep(1000);
        Assert.assertTrue(locators.buttonLogueoError(),"Advertencia");
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Politicas de privacidad">

    @And("^Se consultan las politicas de privacidad$")
    public void consultaPoliticasSeguridad() throws Throwable{
        Thread.sleep(2000);
        locators.clickPoliticasPrivacidad();
    }

    @And("^Se valida el titulo de la imagen sobre las politicas de privacidad \"([^\"]*)\"$")
    public void validaImagenPoliticasPrivacidad(String toltip) throws Throwable{
        Thread.sleep(1000);
        situarDriverEnPestana();
        Assert.assertEquals(locators.getToolTip(), toltip, "Comsatel - Seguridad y Control Satelital");
        Thread.sleep(5000);
    }

    @Then("^Se validan las politicas de privacidad \"([^\"]*)\"$")
    public void validaPoliticasPrivacidad(String politicasComsatel) throws Throwable {
        situarDriverEnPestana();
        Assert.assertEquals(locators.getPoliticasComsatel(), politicasComsatel, "Comsatel Perú SAC - Todos los derechos reservados 2018");
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Sobre Comsatel">

    @And("^Se consulta sobre Comsatel$")
    public void consultaSobreComsatel() throws Throwable{
        Thread.sleep(2000);
        locators.clickSobreComsatel();
    }

    @And("^Se valida el titulo de la imagen Sobre Comsatel \"([^\"]*)\"$")
    public void validaImagenSobreComsatel(String toltip) throws Throwable{
        Thread.sleep(1000);
        situarDriverEnPestana();
        Assert.assertEquals(locators.getToolTip(), toltip, "Comsatel - Control y Seguridad Satelital");
        Thread.sleep(5000);
    }

    @Then("^Se valida sobre Comsatel \"([^\"]*)\"$")
    public void validacionSobreComsatel(String sobreComsatel) throws Throwable {
        situarDriverEnPestana();
        Assert.assertEquals(locators.getPoliticasComsatel(), sobreComsatel, "Comsatel Perú SAC - Todos los derechos reservados 2018");
    }


    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Exportar CLSeguridad Dashboard">
    @And("^Se exportan la asignación de flotas por usuario$")
    public void exportacionAsignacionFlotarPorUsuarios() throws Throwable {
        locators.clickPuntosFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickExportarFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickExportarExcelFlotasPorUsuarios();
        Thread.sleep(3000);
        locators.clickExportarPdfFlotasPorUsuarios();
        Thread.sleep(3000);
        locators.clickCerrarFlotasPorUsuarios();
        Thread.sleep(2000);
    }

    @And("^Se exporta Usuarios y vehículos por flota$")
    public void exportacionUsuariosYVehiculosPorFlota() throws Throwable {
        locators.clickPuntosUsuariosYVehiculosPorFlota();
        Thread.sleep(2000);
        locators.clickExportarUsuariosYVehiculosPorFlota();
        Thread.sleep(2000);
        locators.clickExportarExcelUsuariosYVehiculosPorFlota();
        Thread.sleep(3000);
        locators.clickExportarPdfUsuariosYVehiculosPorFlota();
        Thread.sleep(3000);
        locators.clickCerrarUsuariosYVehiculosPorFlota();
        Thread.sleep(2000);
    }

    @And("^Se exporta Control de usuarios por turno$")
    public void exportacionControlUsuariosPorTurno() throws Throwable {
        locators.clickPuntosControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickExportarControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickExportarExcelControlUsuariosPorTurno();
        Thread.sleep(3000);
        locators.clickExportarPdfControlUsuariosPorTurno();
        Thread.sleep(3000);
        locators.clickCerrarControlUsuariosPorTurno();
        Thread.sleep(2000);
    }

    @And("^Se exporta Control de perfiles por usuario$")
    public void exportacionControlPerfilesPorUsuario() throws Throwable {
        locators.clickPuntosControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickExportarControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickExportarExcelControlPerfilesPorUsuario();
        Thread.sleep(3000);
        locators.clickExportarPdfControlPerfilesPorUsuario();
        Thread.sleep(3000);
        locators.clickCerrarControlPerfilesPorUsuario();
        Thread.sleep(2000);
    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Dashboard en Excel">
    @And("^Se envia la asignación de flotas por usuario en archivo Excel, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarExcelAsignacionFlotarPorUsuarios(String usuario) throws Throwable {
        locators.clickPuntosFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickEnviarFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarFlotasPorUsuarios(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickEnviarCorreoFlotasPorUsuarios();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Usuarios y vehículos por flota en archivo Excel, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarExcelUsuariosYVehiculosPorFlota(String usuario) throws Throwable {
        locators.clickPuntosUsuariosYVehiculosPorFlota();
        Thread.sleep(2000);
        locators.clickEnviarUsuariosYVehiculosPorFlotas();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarUsuariosYVehiculosPorFlotas(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarUsuariosYVehiculosPorFlotas();
        Thread.sleep(2000);
        locators.clickEnviarCorreoUsuariosYVehiculosPorFlotas();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Control de usuarios por turno en archivo Excel, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarExcelControlUsuariosPorTurno(String usuario) throws Throwable {
        locators.clickPuntosControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickEnviarControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarControlUsuariosPorTurno(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickEnviarCorreoControlUsuariosPorTurno();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Control de perfiles por usuario en archivo Excel, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarExcelControlPerfilesPorUsuario(String usuario) throws Throwable {
        locators.clickPuntosControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickEnviarControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarControlPerfilesPorUsuario(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickEnviarCorreoControlPerfilesPorUsuario();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);

    }



    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Enviar CLSeguridad Dashboard en Pdf">
    @And("^Se envia la asignación de flotas por usuario en archivo Pdf, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarPdflAsignacionFlotarPorUsuarios(String usuario) throws Throwable {
        locators.clickPuntosFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickEnviarFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarFlotasPorUsuarios(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickEnviarPdfFlotasPorUsuarios();
        Thread.sleep(2000);
        locators.clickEnviarCorreoFlotasPorUsuarios();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Usuarios y vehículos por flota en archivo Pdf, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarPdfUsuariosYVehiculosPorFlota(String usuario) throws Throwable {
        locators.clickPuntosUsuariosYVehiculosPorFlota();
        Thread.sleep(2000);
        locators.clickEnviarUsuariosYVehiculosPorFlotas();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarUsuariosYVehiculosPorFlotas(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarUsuariosYVehiculosPorFlotas();
        Thread.sleep(2000);
        locators.clickEnviarPdfUsuariosYVehiculosPorFlotas();
        Thread.sleep(2000);
        locators.clickEnviarCorreoUsuariosYVehiculosPorFlotas();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Control de usuarios por turno en archivo Pdf, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarPdfControlUsuariosPorTurno(String usuario) throws Throwable {
        locators.clickPuntosControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickEnviarControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarControlUsuariosPorTurno(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickEnviarPdfControlUsuariosPorTurno();
        Thread.sleep(2000);
        locators.clickEnviarCorreoControlUsuariosPorTurno();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);
    }

    @And("^Se envia Control de perfiles por usuario en archivo Pdf, se agrega un contacto \"([^\"]*)\" y se valida envio de correo$")
    public void enviarPdfControlPerfilesPorUsuario(String usuario) throws Throwable {
        locators.clickPuntosControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickEnviarControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.insertUsuarioEnviarControlPerfilesPorUsuario(usuario);
        Thread.sleep(2000);
        locators.clickSeleccionarEnviarControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickEnviarPdfControlPerfilesPorUsuario();
        Thread.sleep(2000);
        locators.clickEnviarCorreoControlPerfilesPorUsuario();
        Thread.sleep(1000);
        locators.getEnvioExitoso();
        Thread.sleep(2000);

    }

    //</editor-fold>



    //<editor-fold defaultstate="collapsed" desc="2">

    //</editor-fold>

    @After
    public void tearDown() {
      webDriver.quit();
    }

}
