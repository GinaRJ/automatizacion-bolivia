$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("1logincorrecto.feature");
formatter.feature({
  "line": 1,
  "name": "Logueo Exitoso",
  "description": "",
  "id": "logueo-exitoso",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2561621938,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Logueo al modulo CL",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cl",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "Se carga el modulo de CL",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloCL()"
});
formatter.result({
  "duration": 2250747307,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 7799068899,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1072056077,
  "status": "passed"
});
formatter.after({
  "duration": 680577240,
  "status": "passed"
});
formatter.before({
  "duration": 2123141759,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Logueo al modulo CLSeguridad",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clseguridad",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 4532604606,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 9406686952,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1074687189,
  "status": "passed"
});
formatter.after({
  "duration": 639576826,
  "status": "passed"
});
formatter.before({
  "duration": 2150923179,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Logueo al modulo CLMantenimiento",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clmantenimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "Se carga el modulo de CLMantenimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloMantenimiento()"
});
formatter.result({
  "duration": 3346696730,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1498594133,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1053005115,
  "status": "passed"
});
formatter.after({
  "duration": 640056400,
  "status": "passed"
});
formatter.before({
  "duration": 2150062450,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Logueo al modulo CLAlarma",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clalarma",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "Se carga el modulo de CLAlarma",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAlarma()"
});
formatter.result({
  "duration": 2311597404,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1461585044,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1050248277,
  "status": "passed"
});
formatter.after({
  "duration": 633579596,
  "status": "passed"
});
formatter.before({
  "duration": 2125554133,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Logueo al modulo CLHerramientaControl",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clherramientacontrol",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "Se carga el modulo de CLHerramientaControl",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloHerramientaControl()"
});
formatter.result({
  "duration": 6760005552,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1723582421,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1050013611,
  "status": "passed"
});
formatter.after({
  "duration": 634413588,
  "status": "passed"
});
formatter.before({
  "duration": 2056554130,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Logueo al modulo CLReporte",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clreporte",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 28,
  "name": "Se carga el modulo de CLReporte",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloReporte()"
});
formatter.result({
  "duration": 6186018239,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1952365768,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1052707870,
  "status": "passed"
});
formatter.after({
  "duration": 637867030,
  "status": "passed"
});
formatter.before({
  "duration": 2147291106,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 31,
      "value": "#"
    }
  ],
  "line": 32,
  "name": "Logueo al modulo CLRuta",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clruta",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 33,
  "name": "Se carga el modulo de CLRuta",
  "keyword": "Given "
});
formatter.step({
  "line": 34,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRuta()"
});
formatter.result({
  "duration": 10572007294,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 17026944869,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1145173701,
  "status": "passed"
});
formatter.after({
  "duration": 639352399,
  "status": "passed"
});
formatter.before({
  "duration": 2131109336,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Logueo al modulo CLKilometraje",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clkilometraje",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "Se carga el modulo de CLKilometraje",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloKilometraje()"
});
formatter.result({
  "duration": 2170272808,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1488641985,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1050447388,
  "status": "passed"
});
formatter.after({
  "duration": 636406407,
  "status": "passed"
});
formatter.before({
  "duration": 2192317265,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "Logueo al modulo CLDireccion",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cldireccion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 43,
  "name": "Se carga el modulo de CLDireccion",
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDireccion()"
});
formatter.result({
  "duration": 2257914174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2505615079,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1052537203,
  "status": "passed"
});
formatter.after({
  "duration": 644921256,
  "status": "passed"
});
formatter.before({
  "duration": 2113947087,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Logueo al modulo CLAccesorio",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-claccesorio",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 48,
  "name": "Se carga el modulo de CLAccesorio",
  "keyword": "Given "
});
formatter.step({
  "line": 49,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAccesorio()"
});
formatter.result({
  "duration": 2083892670,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 1475700893,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1055011303,
  "status": "passed"
});
formatter.after({
  "duration": 645688119,
  "status": "passed"
});
formatter.before({
  "duration": 2132678333,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "Logueo al modulo CLControlEquipo",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcontrolequipo",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 53,
  "name": "Se carga el modulo de CLControlEquipo",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlequipo()"
});
formatter.result({
  "duration": 5251494258,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 6153096336,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1165040449,
  "status": "passed"
});
formatter.after({
  "duration": 657129618,
  "status": "passed"
});
formatter.before({
  "duration": 2112931904,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Logueo al modulo CLConvoy",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconvoy",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 58,
  "name": "Se carga el modulo de CLConvoy",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloconvoy()"
});
formatter.result({
  "duration": 17819119684,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 3634964272,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1096921086,
  "status": "passed"
});
formatter.after({
  "duration": 637601074,
  "status": "passed"
});
formatter.before({
  "duration": 2108232595,
  "status": "passed"
});
formatter.scenario({
  "line": 62,
  "name": "Logueo al modulo CLSafe",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clsafe",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 63,
  "name": "Se carga el modulo de CLSafe",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamodulosafe()"
});
formatter.result({
  "duration": 3678305097,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1545171652,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1049431069,
  "status": "passed"
});
formatter.after({
  "duration": 633376502,
  "status": "passed"
});
formatter.before({
  "duration": 2149952654,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "Logueo al modulo CLConductor",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconductor",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 68,
  "name": "Se carga el modulo de CLConductor",
  "keyword": "Given "
});
formatter.step({
  "line": 69,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 70,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductor()"
});
formatter.result({
  "duration": 2234574355,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 1530111448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1052843550,
  "status": "passed"
});
formatter.after({
  "duration": 626163556,
  "status": "passed"
});
formatter.before({
  "duration": 2140926089,
  "status": "passed"
});
formatter.scenario({
  "line": 72,
  "name": "Logueo al modulo CLConductorKP",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconductorkp",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 73,
  "name": "Se carga el modulo de CLConductorKP",
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductorkp()"
});
formatter.result({
  "duration": 2243747694,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 1474520448,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1048779405,
  "status": "passed"
});
formatter.after({
  "duration": 626423254,
  "status": "passed"
});
formatter.before({
  "duration": 2129066172,
  "status": "passed"
});
formatter.scenario({
  "line": 77,
  "name": "Logueo al modulo CLDashboard",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cldashboard",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 78,
  "name": "Se carga el modulo de CLDashboard",
  "keyword": "Given "
});
formatter.step({
  "line": 79,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDashboard()"
});
formatter.result({
  "duration": 6698069162,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 32911423097,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1098826296,
  "status": "passed"
});
formatter.after({
  "duration": 658098721,
  "status": "passed"
});
formatter.before({
  "duration": 2167235224,
  "status": "passed"
});
formatter.scenario({
  "line": 82,
  "name": "Logueo al modulo CLCapa",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcapa",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 83,
  "name": "Se carga el modulo de CLCapa",
  "keyword": "Given "
});
formatter.step({
  "line": 84,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 85,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloCapa()"
});
formatter.result({
  "duration": 2215117491,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 1474371968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1050046606,
  "status": "passed"
});
formatter.after({
  "duration": 625883094,
  "status": "passed"
});
formatter.before({
  "duration": 2107551066,
  "status": "passed"
});
formatter.scenario({
  "line": 87,
  "name": "Logueo al modulo CLComando",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcomando",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 88,
  "name": "Se carga el modulo de CLComando",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "2Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModulComando()"
});
formatter.result({
  "duration": 2027818113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1487813114,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 39
    }
  ],
  "location": "CLocator.validarLogueoExitoso2(String)"
});
formatter.result({
  "duration": 1053170092,
  "status": "passed"
});
formatter.after({
  "duration": 629594243,
  "status": "passed"
});
formatter.before({
  "duration": 2144763816,
  "status": "passed"
});
formatter.scenario({
  "line": 92,
  "name": "Logueo al modulo CLControlVencimiento",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcontrolvencimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 93,
  "name": "Se carga el modulo de CLControlVencimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 94,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlVencimiento()"
});
formatter.result({
  "duration": 2288460964,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2691076091,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1058228655,
  "status": "passed"
});
formatter.after({
  "duration": 643233930,
  "status": "passed"
});
formatter.before({
  "duration": 2107119563,
  "status": "passed"
});
formatter.scenario({
  "line": 97,
  "name": "Logueo al modulo CLRutaControlada",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clrutacontrolada",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 98,
  "name": "Se carga el modulo de CLRutaControlada",
  "keyword": "Given "
});
formatter.step({
  "line": 99,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "Se valida que se logueo exitosamente \"CLocator\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRutaControlada()"
});
formatter.result({
  "duration": 12487995726,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 31381249355,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "CLocator",
      "offset": 38
    }
  ],
  "location": "CLocator.validarLogueoExitoso(String)"
});
formatter.result({
  "duration": 1066630580,
  "status": "passed"
});
formatter.after({
  "duration": 650422983,
  "status": "passed"
});
formatter.uri("2loginincorrecto.feature");
formatter.feature({
  "line": 1,
  "name": "Logueo Exitoso",
  "description": "",
  "id": "logueo-exitoso",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2190853513,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Logueo al modulo CL",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cl",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 3,
  "name": "Se carga el modulo de CL",
  "keyword": "Given "
});
formatter.step({
  "line": 4,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 5,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloCL()"
});
formatter.result({
  "duration": 1962190769,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 4029590204,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1058643091,
  "status": "passed"
});
formatter.after({
  "duration": 638570745,
  "status": "passed"
});
formatter.before({
  "duration": 2094642685,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Logueo al modulo CLSeguridad",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clseguridad",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 1875905352,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 365855781,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1056557828,
  "status": "passed"
});
formatter.after({
  "duration": 632533977,
  "status": "passed"
});
formatter.before({
  "duration": 2077571742,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Logueo al modulo CLMantenimiento",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clmantenimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "Se carga el modulo de CLMantenimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloMantenimiento()"
});
formatter.result({
  "duration": 2041323828,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 366142501,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1059589722,
  "status": "passed"
});
formatter.after({
  "duration": 639392791,
  "status": "passed"
});
formatter.before({
  "duration": 2156980711,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Logueo al modulo CLAlarma",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clalarma",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "Se carga el modulo de CLAlarma",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAlarma()"
});
formatter.result({
  "duration": 2230038602,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 14613571762,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1109277929,
  "status": "passed"
});
formatter.after({
  "duration": 633235987,
  "status": "passed"
});
formatter.before({
  "duration": 2151923002,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "Logueo al modulo CLHerramientaControl",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clherramientacontrol",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "Se carga el modulo de CLHerramientaControl",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloHerramientaControl()"
});
formatter.result({
  "duration": 3445413232,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 6121489136,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1056315765,
  "status": "passed"
});
formatter.after({
  "duration": 635482815,
  "status": "passed"
});
formatter.before({
  "duration": 2112031069,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Logueo al modulo CLReporte",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clreporte",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 28,
  "name": "Se carga el modulo de CLReporte",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloReporte()"
});
formatter.result({
  "duration": 1783665085,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 452718907,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1058787021,
  "status": "passed"
});
formatter.after({
  "duration": 633496254,
  "status": "passed"
});
formatter.before({
  "duration": 2082490642,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "Logueo al modulo CLRuta",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clruta",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 33,
  "name": "Se carga el modulo de CLRuta",
  "keyword": "Given "
});
formatter.step({
  "line": 34,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRuta()"
});
formatter.result({
  "duration": 2123155128,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 434871998,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1056516299,
  "status": "passed"
});
formatter.after({
  "duration": 633416325,
  "status": "passed"
});
formatter.before({
  "duration": 2128161922,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "Logueo al modulo CLKilometraje",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clkilometraje",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "Se carga el modulo de CLKilometraje",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloKilometraje()"
});
formatter.result({
  "duration": 1816185637,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1126014365,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1057971233,
  "status": "passed"
});
formatter.after({
  "duration": 636027242,
  "status": "passed"
});
formatter.before({
  "duration": 2060071857,
  "status": "passed"
});
formatter.scenario({
  "line": 42,
  "name": "Logueo al modulo CLDireccion",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cldireccion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 43,
  "name": "Se carga el modulo de CLDireccion",
  "keyword": "Given "
});
formatter.step({
  "line": 44,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDireccion()"
});
formatter.result({
  "duration": 1839023411,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 368843303,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1059739625,
  "status": "passed"
});
formatter.after({
  "duration": 634768859,
  "status": "passed"
});
formatter.before({
  "duration": 2168544523,
  "status": "passed"
});
formatter.scenario({
  "line": 47,
  "name": "Logueo al modulo CLAccesorio",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-claccesorio",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 48,
  "name": "Se carga el modulo de CLAccesorio",
  "keyword": "Given "
});
formatter.step({
  "line": 49,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAccesorio()"
});
formatter.result({
  "duration": 2675332367,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 32539582315,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1125147094,
  "status": "passed"
});
formatter.after({
  "duration": 649683996,
  "status": "passed"
});
formatter.before({
  "duration": 2097409193,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "Logueo al modulo CLControlEquipo",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcontrolequipo",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 53,
  "name": "Se carga el modulo de CLControlEquipo",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlequipo()"
});
formatter.result({
  "duration": 1864186234,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 510844878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1056568636,
  "status": "passed"
});
formatter.after({
  "duration": 627662863,
  "status": "passed"
});
formatter.before({
  "duration": 2080193468,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Logueo al modulo CLConvoy",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconvoy",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 58,
  "name": "Se carga el modulo de CLConvoy",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloconvoy()"
});
formatter.result({
  "duration": 2019386038,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 571136894,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1055385347,
  "status": "passed"
});
formatter.after({
  "duration": 656925956,
  "status": "passed"
});
formatter.before({
  "duration": 2292955189,
  "status": "passed"
});
formatter.scenario({
  "line": 62,
  "name": "Logueo al modulo CLSafe",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clsafe",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 63,
  "name": "Se carga el modulo de CLSafe",
  "keyword": "Given "
});
formatter.step({
  "line": 64,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamodulosafe()"
});
formatter.result({
  "duration": 1886375473,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 403277882,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1057738273,
  "status": "passed"
});
formatter.after({
  "duration": 634928148,
  "status": "passed"
});
formatter.before({
  "duration": 2116092938,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "Logueo al modulo CLConductor",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconductor",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 68,
  "name": "Se carga el modulo de CLConductor",
  "keyword": "Given "
});
formatter.step({
  "line": 69,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 70,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductor()"
});
formatter.result({
  "duration": 1873485866,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 392228916,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1051473665,
  "status": "passed"
});
formatter.after({
  "duration": 639644239,
  "status": "passed"
});
formatter.before({
  "duration": 2144258074,
  "status": "passed"
});
formatter.scenario({
  "line": 72,
  "name": "Logueo al modulo CLConductorKP",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clconductorkp",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 73,
  "name": "Se carga el modulo de CLConductorKP",
  "keyword": "Given "
});
formatter.step({
  "line": 74,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductorkp()"
});
formatter.result({
  "duration": 1836124066,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 386255010,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1051213683,
  "status": "passed"
});
formatter.after({
  "duration": 628319931,
  "status": "passed"
});
formatter.before({
  "duration": 2066671824,
  "status": "passed"
});
formatter.scenario({
  "line": 77,
  "name": "Logueo al modulo CLDashboard",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-cldashboard",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 78,
  "name": "Se carga el modulo de CLDashboard",
  "keyword": "Given "
});
formatter.step({
  "line": 79,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDashboard()"
});
formatter.result({
  "duration": 7385591117,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 23468055022,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1126738561,
  "status": "passed"
});
formatter.after({
  "duration": 633227170,
  "status": "passed"
});
formatter.before({
  "duration": 2203912080,
  "status": "passed"
});
formatter.scenario({
  "line": 82,
  "name": "Logueo al modulo CLCapa",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcapa",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 83,
  "name": "Se carga el modulo de CLCapa",
  "keyword": "Given "
});
formatter.step({
  "line": 84,
  "name": "2Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 85,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloCapa()"
});
formatter.result({
  "duration": 2102823596,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "123",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2868732228,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1056895179,
  "status": "passed"
});
formatter.after({
  "duration": 632960360,
  "status": "passed"
});
formatter.before({
  "duration": 2110550819,
  "status": "passed"
});
formatter.scenario({
  "line": 87,
  "name": "Logueo al modulo CLComando",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcomando",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 88,
  "name": "Se carga el modulo de CLComando",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModulComando()"
});
formatter.result({
  "duration": 1965449367,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 439088321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1059594273,
  "status": "passed"
});
formatter.after({
  "duration": 635348273,
  "status": "passed"
});
formatter.before({
  "duration": 2093845671,
  "status": "passed"
});
formatter.scenario({
  "line": 92,
  "name": "Logueo al modulo CLControlVencimiento",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clcontrolvencimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 93,
  "name": "Se carga el modulo de CLControlVencimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 94,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlVencimiento()"
});
formatter.result({
  "duration": 1915983597,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 403048905,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1057793171,
  "status": "passed"
});
formatter.after({
  "duration": 631241177,
  "status": "passed"
});
formatter.before({
  "duration": 2133343365,
  "status": "passed"
});
formatter.scenario({
  "line": 97,
  "name": "Logueo al modulo CLRutaControlada",
  "description": "",
  "id": "logueo-exitoso;logueo-al-modulo-clrutacontrolada",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 98,
  "name": "Se carga el modulo de CLRutaControlada",
  "keyword": "Given "
});
formatter.step({
  "line": 99,
  "name": "Ingresamos el usuario \"grios\" y password \"123\"",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "Se valida que muestra mensaje de error \"Advertencia\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRutaControlada()"
});
formatter.result({
  "duration": 1826609110,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "123",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 364688989,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Advertencia",
      "offset": 40
    }
  ],
  "location": "CLocator.validarLogueoerroneo(String)"
});
formatter.result({
  "duration": 1055829934,
  "status": "passed"
});
formatter.after({
  "duration": 633148946,
  "status": "passed"
});
formatter.uri("3politicas_de_privacidad.feature");
formatter.feature({
  "line": 1,
  "name": "Consultar Politicas de Privacidad",
  "description": "",
  "id": "consultar-politicas-de-privacidad",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2182306806,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Se consultan las Politicas de Privacidad del modulo CL",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-cl",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Se carga el modulo de CL",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloCL()"
});
formatter.result({
  "duration": 2179263818,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2910747541,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2286816021,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7197104752,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 176792563,
  "status": "passed"
});
formatter.after({
  "duration": 787563114,
  "status": "passed"
});
formatter.before({
  "duration": 2085750378,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Se consultan las Politicas de Privacidad del modulo CLSeguridad",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clseguridad",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 11,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 4920700257,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 14526167055,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2164778476,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7205454908,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 197691562,
  "status": "passed"
});
formatter.after({
  "duration": 699993999,
  "status": "passed"
});
formatter.before({
  "duration": 2120922238,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Se consultan las Politicas de Privacidad del modulo CLMantenimiento",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clmantenimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "Se carga el modulo de CLMantenimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloMantenimiento()"
});
formatter.result({
  "duration": 1753144179,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 746584318,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2093527378,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7168205465,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 183155873,
  "status": "passed"
});
formatter.after({
  "duration": 672833521,
  "status": "passed"
});
formatter.before({
  "duration": 2156816587,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Se consultan las Politicas de Privacidad del modulo CLAlarma",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clalarma",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "Se carga el modulo de CLAlarma",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAlarma()"
});
formatter.result({
  "duration": 1941801496,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 744276619,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2092257617,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7175155868,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 181550183,
  "status": "passed"
});
formatter.after({
  "duration": 682167285,
  "status": "passed"
});
formatter.before({
  "duration": 2167859295,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Se consultan las Politicas de Privacidad del modulo CLHerramientaControl",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clherramientacontrol",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 32,
  "name": "Se carga el modulo de CLHerramientaControl",
  "keyword": "Given "
});
formatter.step({
  "line": 33,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloHerramientaControl()"
});
formatter.result({
  "duration": 1799397433,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 952252844,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2090886594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7173937877,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 182639891,
  "status": "passed"
});
formatter.after({
  "duration": 677456314,
  "status": "passed"
});
formatter.before({
  "duration": 2049912348,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Se consultan las Politicas de Privacidad del modulo CLReporte",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clreporte",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 39,
  "name": "Se carga el modulo de CLReporte",
  "keyword": "Given "
});
formatter.step({
  "line": 40,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloReporte()"
});
formatter.result({
  "duration": 5770066927,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2595696410,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2108308826,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7510037339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 191848216,
  "status": "passed"
});
formatter.after({
  "duration": 684810061,
  "status": "passed"
});
formatter.before({
  "duration": 2159086171,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Se consultan las Politicas de Privacidad del modulo CLRuta",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clruta",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 46,
  "name": "Se carga el modulo de CLRuta",
  "keyword": "Given "
});
formatter.step({
  "line": 47,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRuta()"
});
formatter.result({
  "duration": 1875964516,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 766248258,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2091778328,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7169563118,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 186174399,
  "status": "passed"
});
formatter.after({
  "duration": 663456804,
  "status": "passed"
});
formatter.before({
  "duration": 2125360143,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "Se consultan las Politicas de Privacidad del modulo CLKilometraje",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clkilometraje",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 53,
  "name": "Se carga el modulo de CLKilometraje",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloKilometraje()"
});
formatter.result({
  "duration": 1861601486,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 805053898,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2094166809,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7178202839,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 184929101,
  "status": "passed"
});
formatter.after({
  "duration": 671455956,
  "status": "passed"
});
formatter.before({
  "duration": 2139883599,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "Se consultan las Politicas de Privacidad del modulo CLDireccion",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-cldireccion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 60,
  "name": "Se carga el modulo de CLDireccion",
  "keyword": "Given "
});
formatter.step({
  "line": 61,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDireccion()"
});
formatter.result({
  "duration": 2107823848,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 20596364570,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2172416667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7161399559,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 265149022,
  "status": "passed"
});
formatter.after({
  "duration": 696507276,
  "status": "passed"
});
formatter.before({
  "duration": 2147598022,
  "status": "passed"
});
formatter.scenario({
  "line": 66,
  "name": "Se consultan las Politicas de Privacidad del modulo CLAccesorio",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-claccesorio",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 67,
  "name": "Se carga el modulo de CLAccesorio",
  "keyword": "Given "
});
formatter.step({
  "line": 68,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAccesorio()"
});
formatter.result({
  "duration": 1925957646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 840102593,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2086078058,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7188404445,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 199760896,
  "status": "passed"
});
formatter.after({
  "duration": 677654572,
  "status": "passed"
});
formatter.before({
  "duration": 2134438761,
  "status": "passed"
});
formatter.scenario({
  "line": 73,
  "name": "Se consultan las Politicas de Privacidad del modulo CLControlEquipo",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clcontrolequipo",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 74,
  "name": "Se carga el modulo de CLControlEquipo",
  "keyword": "Given "
});
formatter.step({
  "line": 75,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 76,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 78,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlequipo()"
});
formatter.result({
  "duration": 1662055346,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 5212534449,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2091686452,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7168947865,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 299612900,
  "status": "passed"
});
formatter.after({
  "duration": 671215031,
  "status": "passed"
});
formatter.before({
  "duration": 2170353022,
  "status": "passed"
});
formatter.scenario({
  "line": 80,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConvoy",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clconvoy",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 81,
  "name": "Se carga el modulo de CLConvoy",
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloconvoy()"
});
formatter.result({
  "duration": 2649029489,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 16555645187,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2173822108,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7168543101,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 254911861,
  "status": "passed"
});
formatter.after({
  "duration": 723281478,
  "status": "passed"
});
formatter.before({
  "duration": 2301526928,
  "status": "passed"
});
formatter.scenario({
  "line": 87,
  "name": "Se consultan las Politicas de Privacidad del modulo CLSafe",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clsafe",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 88,
  "name": "Se carga el modulo de CLSafe",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 91,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 92,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamodulosafe()"
});
formatter.result({
  "duration": 1755992608,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 789861143,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2090645954,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7168112736,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 181509508,
  "status": "passed"
});
formatter.after({
  "duration": 664817303,
  "status": "passed"
});
formatter.before({
  "duration": 2163457799,
  "status": "passed"
});
formatter.scenario({
  "line": 94,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConductor",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clconductor",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 95,
  "name": "Se carga el modulo de CLConductor",
  "keyword": "Given "
});
formatter.step({
  "line": 96,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductor()"
});
formatter.result({
  "duration": 5854903668,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2110538018,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2087160938,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7202112968,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 253142899,
  "status": "passed"
});
formatter.after({
  "duration": 667192984,
  "status": "passed"
});
formatter.before({
  "duration": 2149066325,
  "status": "passed"
});
formatter.scenario({
  "line": 101,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConductorKP",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clconductorkp",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 102,
  "name": "Se carga el modulo de CLConductorKP",
  "keyword": "Given "
});
formatter.step({
  "line": 103,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 104,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 105,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 106,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductorkp()"
});
formatter.result({
  "duration": 9854296664,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 11132043453,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2136584326,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7166390992,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 256033426,
  "status": "passed"
});
formatter.after({
  "duration": 692154705,
  "status": "passed"
});
formatter.before({
  "duration": 2130083630,
  "status": "passed"
});
formatter.scenario({
  "line": 108,
  "name": "Se consultan las Politicas de Privacidad del modulo CLDashboard",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-cldashboard",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 109,
  "name": "Se carga el modulo de CLDashboard",
  "keyword": "Given "
});
formatter.step({
  "line": 110,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 111,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 113,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDashboard()"
});
formatter.result({
  "duration": 1842122151,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 3164374564,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2093228426,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7598403754,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 184922274,
  "status": "passed"
});
formatter.after({
  "duration": 667113908,
  "status": "passed"
});
formatter.before({
  "duration": 2109261715,
  "status": "passed"
});
formatter.scenario({
  "line": 115,
  "name": "Se consultan las Politicas de Privacidad del modulo CLCapa",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clcapa",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 116,
  "name": "Se carga el modulo de CLCapa",
  "keyword": "Given "
});
formatter.step({
  "line": 117,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 118,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloCapa()"
});
formatter.result({
  "duration": 3179232244,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 753178313,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2092084675,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7180132227,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 185175999,
  "status": "passed"
});
formatter.after({
  "duration": 717077457,
  "status": "passed"
});
formatter.before({
  "duration": 2214240833,
  "status": "passed"
});
formatter.scenario({
  "line": 122,
  "name": "Se consultan las Politicas de Privacidad del modulo CLComando",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clcomando",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 123,
  "name": "Se carga el modulo de CLComando",
  "keyword": "Given "
});
formatter.step({
  "line": 124,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 125,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModulComando()"
});
formatter.result({
  "duration": 1886104113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 15589768584,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2178690093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7176817878,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 255447185,
  "status": "passed"
});
formatter.after({
  "duration": 689344676,
  "status": "passed"
});
formatter.before({
  "duration": 2125080249,
  "status": "passed"
});
formatter.scenario({
  "line": 129,
  "name": "Se consultan las Politicas de Privacidad del modulo CLControlVencimiento",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clcontrolvencimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 130,
  "name": "Se carga el modulo de CLControlVencimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 131,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 132,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 133,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 134,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlVencimiento()"
});
formatter.result({
  "duration": 1884903757,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1359323511,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2099867079,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7177020402,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 179246182,
  "status": "passed"
});
formatter.after({
  "duration": 677397150,
  "status": "passed"
});
formatter.before({
  "duration": 2054349969,
  "status": "passed"
});
formatter.scenario({
  "line": 136,
  "name": "Se consultan las Politicas de Privacidad del modulo CLRutaControlada",
  "description": "",
  "id": "consultar-politicas-de-privacidad;se-consultan-las-politicas-de-privacidad-del-modulo-clrutacontrolada",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 137,
  "name": "Se carga el modulo de CLRutaControlada",
  "keyword": "Given "
});
formatter.step({
  "line": 138,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 139,
  "name": "Se consultan las politicas de privacidad",
  "keyword": "And "
});
formatter.step({
  "line": 140,
  "name": "Se valida el titulo de la imagen sobre las politicas de privacidad \"Comsatel - Seguridad y Control Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 141,
  "name": "Se validan las politicas de privacidad \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRutaControlada()"
});
formatter.result({
  "duration": 1799404828,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 6792472913,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaPoliticasSeguridad()"
});
formatter.result({
  "duration": 2136232469,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Seguridad y Control Satelital",
      "offset": 68
    }
  ],
  "location": "CLocator.validaImagenPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 7174264135,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 40
    }
  ],
  "location": "CLocator.validaPoliticasPrivacidad(String)"
});
formatter.result({
  "duration": 230394727,
  "status": "passed"
});
formatter.after({
  "duration": 687866703,
  "status": "passed"
});
formatter.uri("4sobre_comsatel.feature");
formatter.feature({
  "line": 1,
  "name": "Consultar Sobre comsatel",
  "description": "",
  "id": "consultar-sobre-comsatel",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2099887559,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Se consulta sobre comsatel del modulo CL",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consulta-sobre-comsatel-del-modulo-cl",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Se carga el modulo de CL",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloCL()"
});
formatter.result({
  "duration": 3486203725,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 15270339282,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2284367807,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7692125407,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 262081003,
  "status": "passed"
});
formatter.after({
  "duration": 728122441,
  "status": "passed"
});
formatter.before({
  "duration": 2090428070,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Se consultan las Politicas de Privacidad del modulo CLSeguridad",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clseguridad",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 11,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 1777281011,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2600406528,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2111558321,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7167019046,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 205679335,
  "status": "passed"
});
formatter.after({
  "duration": 678311924,
  "status": "passed"
});
formatter.before({
  "duration": 2278858114,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Se consultan las Politicas de Privacidad del modulo CLMantenimiento",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clmantenimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 18,
  "name": "Se carga el modulo de CLMantenimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloMantenimiento()"
});
formatter.result({
  "duration": 1823482780,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2145092918,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2092103732,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7163762724,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 187472605,
  "status": "passed"
});
formatter.after({
  "duration": 665122512,
  "status": "passed"
});
formatter.before({
  "duration": 2117134574,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Se consultan las Politicas de Privacidad del modulo CLAlarma",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clalarma",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "Se carga el modulo de CLAlarma",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAlarma()"
});
formatter.result({
  "duration": 1874225707,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2404900924,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2091322648,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7166065019,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 185219518,
  "status": "passed"
});
formatter.after({
  "duration": 665904165,
  "status": "passed"
});
formatter.before({
  "duration": 2104313802,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Se consultan las Politicas de Privacidad del modulo CLHerramientaControl",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clherramientacontrol",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 32,
  "name": "Se carga el modulo de CLHerramientaControl",
  "keyword": "Given "
});
formatter.step({
  "line": 33,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloHerramientaControl()"
});
formatter.result({
  "duration": 1764241785,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2457227069,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2087415232,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7291308255,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 196839650,
  "status": "passed"
});
formatter.after({
  "duration": 680145170,
  "status": "passed"
});
formatter.before({
  "duration": 2203980062,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "Se consultan las Politicas de Privacidad del modulo CLReporte",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clreporte",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 39,
  "name": "Se carga el modulo de CLReporte",
  "keyword": "Given "
});
formatter.step({
  "line": 40,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 41,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloReporte()"
});
formatter.result({
  "duration": 1819422049,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1241447675,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2090253136,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7584399692,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 185914702,
  "status": "passed"
});
formatter.after({
  "duration": 663927844,
  "status": "passed"
});
formatter.before({
  "duration": 2124116266,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "Se consultan las Politicas de Privacidad del modulo CLRuta",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clruta",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 46,
  "name": "Se carga el modulo de CLRuta",
  "keyword": "Given "
});
formatter.step({
  "line": 47,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 50,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRuta()"
});
formatter.result({
  "duration": 3105060469,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 781543983,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2086262947,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7171736844,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 183788478,
  "status": "passed"
});
formatter.after({
  "duration": 665538939,
  "status": "passed"
});
formatter.before({
  "duration": 2168043900,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "Se consultan las Politicas de Privacidad del modulo CLKilometraje",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clkilometraje",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 53,
  "name": "Se carga el modulo de CLKilometraje",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloKilometraje()"
});
formatter.result({
  "duration": 1754903469,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 4630761603,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2089330113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7306587481,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 288879666,
  "status": "passed"
});
formatter.after({
  "duration": 674570340,
  "status": "passed"
});
formatter.before({
  "duration": 2075283669,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "Se consultan las Politicas de Privacidad del modulo CLDireccion",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-cldireccion",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 60,
  "name": "Se carga el modulo de CLDireccion",
  "keyword": "Given "
});
formatter.step({
  "line": 61,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDireccion()"
});
formatter.result({
  "duration": 1852989641,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 3053916475,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2090066256,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7551584740,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 182045970,
  "status": "passed"
});
formatter.after({
  "duration": 676899940,
  "status": "passed"
});
formatter.before({
  "duration": 2118646681,
  "status": "passed"
});
formatter.scenario({
  "line": 66,
  "name": "Se consultan las Politicas de Privacidad del modulo CLAccesorio",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-claccesorio",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 67,
  "name": "Se carga el modulo de CLAccesorio",
  "keyword": "Given "
});
formatter.step({
  "line": 68,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 69,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 70,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 71,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloAccesorio()"
});
formatter.result({
  "duration": 3931960850,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2071688858,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2090338185,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7452495618,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 183652229,
  "status": "passed"
});
formatter.after({
  "duration": 674762055,
  "status": "passed"
});
formatter.before({
  "duration": 2102098263,
  "status": "passed"
});
formatter.scenario({
  "line": 73,
  "name": "Se consultan las Politicas de Privacidad del modulo CLControlEquipo",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clcontrolequipo",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 74,
  "name": "Se carga el modulo de CLControlEquipo",
  "keyword": "Given "
});
formatter.step({
  "line": 75,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 76,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 78,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlequipo()"
});
formatter.result({
  "duration": 1825043811,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 2431223427,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2087943161,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7481029393,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 185767928,
  "status": "passed"
});
formatter.after({
  "duration": 662257870,
  "status": "passed"
});
formatter.before({
  "duration": 2102419969,
  "status": "passed"
});
formatter.scenario({
  "line": 80,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConvoy",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clconvoy",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 81,
  "name": "Se carga el modulo de CLConvoy",
  "keyword": "Given "
});
formatter.step({
  "line": 82,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamoduloconvoy()"
});
formatter.result({
  "duration": 5901732922,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 6588524424,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2155836960,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7187544569,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 294526461,
  "status": "passed"
});
formatter.after({
  "duration": 677080279,
  "status": "passed"
});
formatter.before({
  "duration": 2172316258,
  "status": "passed"
});
formatter.scenario({
  "line": 87,
  "name": "Se consultan las Politicas de Privacidad del modulo CLSafe",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clsafe",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 88,
  "name": "Se carga el modulo de CLSafe",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 91,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 92,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargamodulosafe()"
});
formatter.result({
  "duration": 1841597065,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 775927908,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2087040334,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7518023406,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 182876548,
  "status": "passed"
});
formatter.after({
  "duration": 670479173,
  "status": "passed"
});
formatter.before({
  "duration": 2040260290,
  "status": "passed"
});
formatter.scenario({
  "line": 94,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConductor",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clconductor",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 95,
  "name": "Se carga el modulo de CLConductor",
  "keyword": "Given "
});
formatter.step({
  "line": 96,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 98,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 99,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductor()"
});
formatter.result({
  "duration": 1769294091,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2328489715,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2086421098,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7170988755,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 185317936,
  "status": "passed"
});
formatter.after({
  "duration": 662632199,
  "status": "passed"
});
formatter.before({
  "duration": 2037401905,
  "status": "passed"
});
formatter.scenario({
  "line": 101,
  "name": "Se consultan las Politicas de Privacidad del modulo CLConductorKP",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clconductorkp",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 102,
  "name": "Se carga el modulo de CLConductorKP",
  "keyword": "Given "
});
formatter.step({
  "line": 103,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 104,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 105,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 106,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloConductorkp()"
});
formatter.result({
  "duration": 3323148415,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 2256861444,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2080325450,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7197372414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 183150469,
  "status": "passed"
});
formatter.after({
  "duration": 662269532,
  "status": "passed"
});
formatter.before({
  "duration": 2080161041,
  "status": "passed"
});
formatter.scenario({
  "line": 108,
  "name": "Se consultan las Politicas de Privacidad del modulo CLDashboard",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-cldashboard",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 109,
  "name": "Se carga el modulo de CLDashboard",
  "keyword": "Given "
});
formatter.step({
  "line": 110,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 111,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 112,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 113,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloDashboard()"
});
formatter.result({
  "duration": 1772897720,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 4246368282,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2093044675,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7168976594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 255123203,
  "status": "passed"
});
formatter.after({
  "duration": 687007964,
  "status": "passed"
});
formatter.before({
  "duration": 2090913900,
  "status": "passed"
});
formatter.scenario({
  "line": 115,
  "name": "Se consultan las Politicas de Privacidad del modulo CLCapa",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clcapa",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 116,
  "name": "Se carga el modulo de CLCapa",
  "keyword": "Given "
});
formatter.step({
  "line": 117,
  "name": "2Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 118,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 120,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloCapa()"
});
formatter.result({
  "duration": 5427603586,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 24
    },
    {
      "val": "team*19",
      "offset": 43
    }
  ],
  "location": "CLocator.InsertUserandPassword2(String,String)"
});
formatter.result({
  "duration": 789631027,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2096099610,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7178973399,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 191157869,
  "status": "passed"
});
formatter.after({
  "duration": 689364019,
  "status": "passed"
});
formatter.before({
  "duration": 2115884439,
  "status": "passed"
});
formatter.scenario({
  "line": 122,
  "name": "Se consultan las Politicas de Privacidad del modulo CLComando",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clcomando",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 123,
  "name": "Se carga el modulo de CLComando",
  "keyword": "Given "
});
formatter.step({
  "line": 124,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 125,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModulComando()"
});
formatter.result({
  "duration": 2784157175,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 10406209653,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2141174694,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7624354773,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 304491693,
  "status": "passed"
});
formatter.after({
  "duration": 674035583,
  "status": "passed"
});
formatter.before({
  "duration": 2101413036,
  "status": "passed"
});
formatter.scenario({
  "line": 129,
  "name": "Se consultan las Politicas de Privacidad del modulo CLControlVencimiento",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clcontrolvencimiento",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 130,
  "name": "Se carga el modulo de CLControlVencimiento",
  "keyword": "Given "
});
formatter.step({
  "line": 131,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 132,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 133,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 134,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloControlVencimiento()"
});
formatter.result({
  "duration": 3511734051,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 1341812248,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2091106470,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7165695525,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 263450603,
  "status": "passed"
});
formatter.after({
  "duration": 665600947,
  "status": "passed"
});
formatter.before({
  "duration": 2193360039,
  "status": "passed"
});
formatter.scenario({
  "line": 136,
  "name": "Se consultan las Politicas de Privacidad del modulo CLRutaControlada",
  "description": "",
  "id": "consultar-sobre-comsatel;se-consultan-las-politicas-de-privacidad-del-modulo-clrutacontrolada",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 137,
  "name": "Se carga el modulo de CLRutaControlada",
  "keyword": "Given "
});
formatter.step({
  "line": 138,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 139,
  "name": "Se consulta sobre Comsatel",
  "keyword": "And "
});
formatter.step({
  "line": 140,
  "name": "Se valida el titulo de la imagen Sobre Comsatel \"Comsatel - Control y Seguridad Satelital\"",
  "keyword": "And "
});
formatter.step({
  "line": 141,
  "name": "Se valida sobre Comsatel \"Comsatel Perú SAC - Todos los derechos reservados 2018\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CLocator.cargaModuloRutaControlada()"
});
formatter.result({
  "duration": 3220076214,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 8762858816,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.consultaSobreComsatel()"
});
formatter.result({
  "duration": 2173365290,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel - Control y Seguridad Satelital",
      "offset": 49
    }
  ],
  "location": "CLocator.validaImagenSobreComsatel(String)"
});
formatter.result({
  "duration": 7561041671,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Comsatel Perú SAC - Todos los derechos reservados 2018",
      "offset": 26
    }
  ],
  "location": "CLocator.validacionSobreComsatel(String)"
});
formatter.result({
  "duration": 255200288,
  "status": "passed"
});
formatter.after({
  "duration": 715724638,
  "status": "passed"
});
formatter.uri("5CLSeguridad_exportar_enviar.feature");
formatter.feature({
  "line": 1,
  "name": "Exportar y enviar archivos del modulo CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2043266869,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Se exportan los cards del Dashboard de CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-exportan-los-cards-del-dashboard-de-clseguridad",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "Se exportan la asignación de flotas por usuario",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Se exporta Usuarios y vehículos por flota",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Se exporta Control de usuarios por turno",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Se exporta Control de perfiles por usuario",
  "keyword": "And "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 3312794916,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 8293335491,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.exportacionAsignacionFlotarPorUsuarios()"
});
formatter.result({
  "duration": 24411835683,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.exportacionUsuariosYVehiculosPorFlota()"
});
formatter.result({
  "duration": 16044911243,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.exportacionControlUsuariosPorTurno()"
});
formatter.result({
  "duration": 20343470560,
  "status": "passed"
});
formatter.match({
  "location": "CLocator.exportacionControlPerfilesPorUsuario()"
});
formatter.result({
  "duration": 30510176148,
  "status": "passed"
});
formatter.after({
  "duration": 697097783,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 11,
  "name": "Se envian los cards en archivo Excel del Dashboard de CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-excel-del-dashboard-de-clseguridad",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 12,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Se envia la asignación de flotas por usuario en archivo Excel, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Se envia Usuarios y vehículos por flota en archivo Excel, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Se envia Control de usuarios por turno en archivo Excel, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Se envia Control de perfiles por usuario en archivo Excel, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.examples({
  "line": 18,
  "name": "",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-excel-del-dashboard-de-clseguridad;",
  "rows": [
    {
      "cells": [
        "correo"
      ],
      "line": 19,
      "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-excel-del-dashboard-de-clseguridad;;1"
    },
    {
      "cells": [
        "grios@comsatel.com.pe"
      ],
      "line": 20,
      "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-excel-del-dashboard-de-clseguridad;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 2085011106,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Se envian los cards en archivo Excel del Dashboard de CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-excel-del-dashboard-de-clseguridad;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 12,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "Se envia la asignación de flotas por usuario en archivo Excel, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "Se envia Usuarios y vehículos por flota en archivo Excel, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "Se envia Control de usuarios por turno en archivo Excel, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "Se envia Control de perfiles por usuario en archivo Excel, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 2182645010,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 4104842015,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 86
    }
  ],
  "location": "CLocator.enviarExcelAsignacionFlotarPorUsuarios(String)"
});
formatter.result({
  "duration": 14445007613,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 81
    }
  ],
  "location": "CLocator.enviarExcelUsuariosYVehiculosPorFlota(String)"
});
formatter.result({
  "duration": 14017407156,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 80
    }
  ],
  "location": "CLocator.enviarExcelControlUsuariosPorTurno(String)"
});
formatter.result({
  "duration": 20439227130,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 82
    }
  ],
  "location": "CLocator.enviarExcelControlPerfilesPorUsuario(String)"
});
formatter.result({
  "duration": 14474173994,
  "status": "passed"
});
formatter.after({
  "duration": 659006668,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 22,
  "name": "Se envian los cards en archivo Pdf del Dashboard de CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-pdf-del-dashboard-de-clseguridad",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Se envia la asignación de flotas por usuario en archivo Pdf, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "Se envia Usuarios y vehículos por flota en archivo Pdf, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "Se envia Control de usuarios por turno en archivo Pdf, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Se envia Control de perfiles por usuario en archivo Pdf, se agrega un contacto \"\u003ccorreo\u003e\" y se valida envio de correo",
  "keyword": "And "
});
formatter.examples({
  "line": 29,
  "name": "",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-pdf-del-dashboard-de-clseguridad;",
  "rows": [
    {
      "cells": [
        "correo"
      ],
      "line": 30,
      "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-pdf-del-dashboard-de-clseguridad;;1"
    },
    {
      "cells": [
        "grios@comsatel.com.pe"
      ],
      "line": 31,
      "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-pdf-del-dashboard-de-clseguridad;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 2186982790,
  "status": "passed"
});
formatter.scenario({
  "line": 31,
  "name": "Se envian los cards en archivo Pdf del Dashboard de CLSeguridad",
  "description": "",
  "id": "exportar-y-enviar-archivos-del-modulo-clseguridad;se-envian-los-cards-en-archivo-pdf-del-dashboard-de-clseguridad;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 23,
  "name": "Se carga el modulo de CLSeguridad",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "Ingresamos el usuario \"grios\" y password \"team*19\"",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Se envia la asignación de flotas por usuario en archivo Pdf, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "Se envia Usuarios y vehículos por flota en archivo Pdf, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "Se envia Control de usuarios por turno en archivo Pdf, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "Se envia Control de perfiles por usuario en archivo Pdf, se agrega un contacto \"grios@comsatel.com.pe\" y se valida envio de correo",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.match({
  "location": "CLocator.cargaModuloSeguridad()"
});
formatter.result({
  "duration": 2265056293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios",
      "offset": 23
    },
    {
      "val": "team*19",
      "offset": 42
    }
  ],
  "location": "CLocator.insertUserandPassword(String,String)"
});
formatter.result({
  "duration": 5977017160,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 84
    }
  ],
  "location": "CLocator.enviarPdflAsignacionFlotarPorUsuarios(String)"
});
formatter.result({
  "duration": 14953328897,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 79
    }
  ],
  "location": "CLocator.enviarPdfUsuariosYVehiculosPorFlota(String)"
});
formatter.result({
  "duration": 24134924877,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 78
    }
  ],
  "location": "CLocator.enviarPdfControlUsuariosPorTurno(String)"
});
formatter.result({
  "duration": 14981592737,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "grios@comsatel.com.pe",
      "offset": 80
    }
  ],
  "location": "CLocator.enviarPdfControlPerfilesPorUsuario(String)"
});
formatter.result({
  "duration": 17115032243,
  "status": "passed"
});
formatter.after({
  "duration": 647774520,
  "status": "passed"
});
});