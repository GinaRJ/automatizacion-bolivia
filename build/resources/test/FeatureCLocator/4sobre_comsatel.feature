Feature: Consultar Sobre comsatel

  Scenario: Se consulta sobre comsatel del modulo CL
    Given Se carga el modulo de CL
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLSeguridad
    Given Se carga el modulo de CLSeguridad
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLMantenimiento
    Given Se carga el modulo de CLMantenimiento
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLAlarma
    Given Se carga el modulo de CLAlarma
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLHerramientaControl
    Given Se carga el modulo de CLHerramientaControl
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLReporte
    Given Se carga el modulo de CLReporte
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLRuta
    Given Se carga el modulo de CLRuta
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLKilometraje
    Given Se carga el modulo de CLKilometraje
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLDireccion
    Given Se carga el modulo de CLDireccion
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLAccesorio
    Given Se carga el modulo de CLAccesorio
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLControlEquipo
    Given Se carga el modulo de CLControlEquipo
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLConvoy
    Given Se carga el modulo de CLConvoy
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLSafe
    Given Se carga el modulo de CLSafe
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLConductor
    Given Se carga el modulo de CLConductor
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLConductorKP
    Given Se carga el modulo de CLConductorKP
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLDashboard
    Given Se carga el modulo de CLDashboard
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLCapa
    Given Se carga el modulo de CLCapa
    When 2Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLComando
    Given Se carga el modulo de CLComando
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLControlVencimiento
    Given Se carga el modulo de CLControlVencimiento
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"

  Scenario: Se consultan las Politicas de Privacidad del modulo CLRutaControlada
    Given Se carga el modulo de CLRutaControlada
    When Ingresamos el usuario "grios" y password "team*19"
    And Se consulta sobre Comsatel
    And Se valida el titulo de la imagen Sobre Comsatel "Comsatel - Control y Seguridad Satelital"
    Then Se valida sobre Comsatel "Comsatel Perú SAC - Todos los derechos reservados 2018"


