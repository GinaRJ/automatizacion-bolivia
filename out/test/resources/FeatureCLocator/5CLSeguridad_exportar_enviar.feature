Feature: Exportar y enviar archivos del modulo CLSeguridad

#  Scenario: Se exportan los cards del Dashboard de CLSeguridad
#    Given Se carga el modulo de CLSeguridad
#    When Ingresamos el usuario "grios" y password "team*19"
#    And Se exportan la asignación de flotas por usuario
#    And Se exporta Usuarios y vehículos por flota
#    And Se exporta Control de usuarios por turno
#    And Se exporta Control de perfiles por usuario

  Scenario Outline: Se envian los cards en archivo Excel del Dashboard de CLSeguridad
    Given Se carga el modulo de CLSeguridad
    When Ingresamos el usuario "grios" y password "team*19"
    And Se envia la asignación de flotas por usuario en archivo Excel, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Usuarios y vehículos por flota en archivo Excel, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Control de usuarios por turno en archivo Excel, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Control de perfiles por usuario en archivo Excel, se agrega un contacto "<correo>" y se valida envio de correo
    Examples:
      | correo                |
      | grios@comsatel.com.pe |

  Scenario Outline: Se envian los cards en archivo Pdf del Dashboard de CLSeguridad
    Given Se carga el modulo de CLSeguridad
    When Ingresamos el usuario "grios" y password "team*19"
    And Se envia la asignación de flotas por usuario en archivo Pdf, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Usuarios y vehículos por flota en archivo Pdf, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Control de usuarios por turno en archivo Pdf, se agrega un contacto "<correo>" y se valida envio de correo
    And Se envia Control de perfiles por usuario en archivo Pdf, se agrega un contacto "<correo>" y se valida envio de correo
    Examples:
      | correo                |
      | grios@comsatel.com.pe |




